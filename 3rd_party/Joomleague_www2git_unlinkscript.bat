@echo off

set GIT=C:\Users\boss\Documents\git\joomleague\
set WWW=C:\xampp\htdocs\jl25
set WWWADMIN=C:\xampp\htdocs\jl25\administrator

REM #####################
REM # Standard Component
REM #####################
echo remove components\com_joomleague
rmdir /S /Q %WWW%\components\com_joomleague
echo link components\com_joomleague with GIT
REM REM mklink /D %WWW%\components\com_joomleague %GIT%\com_joomleague\components\com_joomleague\ 
      
echo remove administrator\components\com_joomleague
rmdir /S /Q %WWW%\administrator\components\com_joomleague
echo link administrator\components\com_joomleague with GIT
REM REM mklink /D %WWW%\administrator\components\com_joomleague %GIT%\com_joomleague\administrator\components\com_joomleague\ 

REM #language en-GB
echo remove language\en-GB\en-GB.com_joomleague.ini
del /F /Q %WWW%\language\en-GB\en-GB.com_joomleague.ini
echo link com_joomleague\language\en-GB\en-GB.com_joomleague.ini with GIT 
REM mklink %WWW%\language\en-GB\en-GB.com_joomleague.ini %GIT%\com_joomleague\components\com_joomleague\language\en-GB\en-GB.com_joomleague.ini

echo remove administrator\language\en-GB\en-GB.com_joomleague.ini
del /F /Q %WWW%\administrator\language\en-GB\en-GB.com_joomleague.ini
echo link com_joomleague\administrator\language\en-GB\en-GB.com_joomleague.ini with GIT
REM mklink %WWW%\administrator\language\en-GB\en-GB.com_joomleague.ini %GIT%\com_joomleague\administrator\components\com_joomleague\language\en-GB\en-GB.com_joomleague.ini

echo remove administrator\language\en-GB\en-GB.com_joomleague_countries.ini
del /F /Q %WWW%\administrator\language\en-GB\en-GB.com_joomleague_countries.ini
echo link com_joomleague\administrator\language\en-GB\en-GB.com_joomleague_countries.ini with GIT
REM mklink %WWW%\administrator\language\en-GB\en-GB.com_joomleague_countries.ini %GIT%\com_joomleague\administrator\components\com_joomleague\language\en-GB\en-GB.com_joomleague_countries.ini

echo remove administrator\language\en-GB\en-GB.com_joomleague_sport_types.ini
del /F /Q %WWW%\administrator\language\en-GB\en-GB.com_joomleague_sport_types.ini
echo link com_joomleague\administrator\language\en-GB\en-GB.com_joomleague_sport_types.ini with GIT
REM mklink %WWW%\administrator\language\en-GB\en-GB.com_joomleague_sport_types.ini %GIT%\com_joomleague\administrator\components\com_joomleague\language\en-GB\en-GB.com_joomleague_sport_types.ini

REM #Media files
echo remove media
rmdir /S /Q %WWW%\media\com_joomleague
echo link media with GIT
REM REM mklink /D %WWW%\media\com_joomleague %GIT%\com_joomleague\media\com_joomleague

REM #Images files
echo remove images
rmdir /S /Q %WWW%\images\com_joomleague
mkdir %WWW%\images\com_joomleague
rmdir /S /Q %WWW%\images\com_joomleague\database
echo link images with GIT
REM REM mklink /D %WWW%\images\com_joomleague\database %GIT%\com_joomleague\media\com_joomleague\database


REM ##################
REM # Additional Files 
REM ##################
REM # language de-DE
echo remove language file administrator\language\de-DE\de-DE.com_joomleague.ini
del /F /Q %WWW%\administrator\language\de-DE\de-DE.com_joomleague.ini
echo link administrator\language\de-DE\de-DE.com_joomleague.ini with GIT
REM mklink %WWW%\administrator\language\de-DE\de-DE.com_joomleague.ini  %GIT%\administrator\language\de-DE\de-DE.com_joomleague.ini 

echo remove administrator\language\de-DE\de-DE.com_joomleague.sys.ini
del /F /Q %WWW%\administrator\language\de-DE\de-DE.com_joomleague.sys.ini
echo link administrator\language\de-DE\de-DE.com_joomleague.sys.ini with GIT
REM mklink %WWW%\administrator\language\de-DE\de-DE.com_joomleague.sys.ini  %GIT%\administrator\language\de-DE\de-DE.com_joomleague.sys.ini 
                                                                      
echo remove language\de-DE\de-DE.com_joomleague.ini
del /F /Q %WWW%\language\de-DE\de-DE.com_joomleague.ini
echo link language\de-DE\de-DE.com_joomleague.ini with GIT
REM mklink %WWW%\language\de-DE\de-DE.com_joomleague.ini %GIT%\com_joomleague\components\com_joomleague\language\de-DE\de-DE.com_joomleague.ini 

ECHO --> Link Modules
cls
for /F "tokens=4" %%i in ('dir /A:D %GIT%com_joomleague\components\com_joomleague\modules\mod_*') do (
  ECHO rmdir /S /Q %WWW%\modules\%%i
  rmdir /S /Q %WWW%\modules\%%i
  ECHO REM REM mklink /D %WWW%\modules\%%i %GIT%com_joomleague\components\com_joomleague\modules\%%i\
  REM REM mklink /D %WWW%\modules\%%i %GIT%com_joomleague\components\com_joomleague\modules\%%i\
  ECHO REM mklink %WWW%\modules\%%i %GIT%com_joomleague\components\com_joomleague\modules\%%i\
  REM mklink %WWW%\modules\%%i %GIT%com_joomleague\components\com_joomleague\modules\%%i\

  ECHO del %WWW%\language\en-GB\en-GB.%%i.ini
  del %WWW%\language\en-GB\en-GB.%%i.ini
  
  ECHO REM mklink %WWW%\language\en-GB\en-GB.%%i.ini %GIT%com_joomleague\components\com_joomleague\modules\%%i\language\en-GB\en-GB.%%i.ini
  REM mklink %WWW%\language\en-GB\en-GB.%%i.ini %GIT%com_joomleague\components\com_joomleague\modules\%%i\language\en-GB\en-GB.%%i.ini
  
  REM .sys.ini
  ECHO del %WWW%\language\en-GB\en-GB.%%i.sys.ini
  del %WWW%\language\en-GB\en-GB.%%i.sys.ini
  
  ECHO REM mklink %WWW%\language\en-GB\en-GB.%%i.sys.ini %GIT%com_joomleague\components\com_joomleague\modules\%%i\language\en-GB\en-GB.%%i.sys.ini
  REM mklink %WWW%\language\en-GB\en-GB.%%i.sys.ini %GIT%com_joomleague\components\com_joomleague\modules\%%i\language\en-GB\en-GB.%%i.sys.ini
   	
  REM de-DE 
  ECHO del %WWW%\language\de-DE\de-DE.%%i.ini
  del %WWW%\language\de-DE\de-DE.%%i.ini
  
  ECHO REM mklink %WWW%\language\de-DE\de-DE.%%i.ini %GIT%com_joomleague\components\com_joomleague\modules\%%i\language\de-DE\de-DE.%%i.ini
  REM mklink %WWW%\language\de-DE\de-DE.%%i.ini %GIT%com_joomleague\components\com_joomleague\modules\%%i\language\de-DE\de-DE.%%i.ini

  REM .sys.ini
  ECHO del %WWW%\language\de-DE\de-DE.%%i.sys.ini
  del %WWW%\language\de-DE\de-DE.%%i.sys.ini
  
  ECHO REM mklink %WWW%\language\de-DE\de-DE.%%i.sys.ini %GIT%com_joomleague\components\com_joomleague\modules\%%i\language\de-DE\de-DE.%%i.sys.ini
  REM mklink %WWW%\language\de-DE\de-DE.%%i.sys.ini %GIT%com_joomleague\components\com_joomleague\modules\%%i\language\de-DE\de-DE.%%i.sys.ini
 
  del %WWW%\language\en-GB\en-GB.Bytes*
  del %WWW%\language\en-GB\en-GB.C
  del %WWW%\language\de-DE\de-DE.Bytes*
  del %WWW%\language\de-DE\de-DE.C 

  rmdir /S /Q %WWW%\modules\Bytes
  ECHO -- unlinked Module %%i --------------------------------------------
)

ECHO --> Link Administrator Modules
cls
for /F "tokens=4" %%i in ('dir /A:D %GIT%com_joomleague\administrator\components\com_joomleague\modules\mod_*') do (
  ECHO rmdir /S /Q %WWWADMIN%\modules\%%i
  rmdir /S /Q %WWWADMIN%\modules\%%i
  ECHO REM REM mklink /D %WWWADMIN%\modules\%%i %GIT%com_joomleague\administrator\components\com_joomleague\modules\%%i\
  REM REM mklink /D %WWWADMIN%\modules\%%i %GIT%com_joomleague\administrator\components\com_joomleague\modules\%%i\
  ECHO REM mklink %WWWADMIN%\modules\%%i %GIT%com_joomleague\administrator\components\com_joomleague\modules\%%i\
  REM mklink %WWWADMIN%\modules\%%i %GIT%com_joomleague\administrator\components\com_joomleague\modules\%%i\

  ECHO del %WWWADMIN%\language\en-GB\en-GB.%%i.ini
  del %WWWADMIN%\language\en-GB\en-GB.%%i.ini
  
  ECHO REM mklink %WWWADMIN%\language\en-GB\en-GB.%%i.ini %GIT%com_joomleague\administrator\components\com_joomleague\modules\%%i\language\en-GB\en-GB.%%i.ini
  REM mklink %WWWADMIN%\language\en-GB\en-GB.%%i.ini %GIT%com_joomleague\administrator\components\com_joomleague\modules\%%i\language\en-GB\en-GB.%%i.ini
  
  REM .sys.ini
  ECHO del %WWWADMIN%\language\en-GB\en-GB.%%i.sys.ini
  del %WWWADMIN%\language\en-GB\en-GB.%%i.sys.ini
  
  ECHO REM mklink %WWWADMIN%\language\en-GB\en-GB.%%i.sys.ini %GIT%com_joomleague\administrator\components\com_joomleague\modules\%%i\language\en-GB\en-GB.%%i.sys.ini
  REM mklink %WWWADMIN%\language\en-GB\en-GB.%%i.sys.ini %GIT%com_joomleague\administrator\components\com_joomleague\modules\%%i\language\en-GB\en-GB.%%i.sys.ini
   	
  REM de-DE 
  ECHO del %WWWADMIN%\language\de-DE\de-DE.%%i.ini
  del %WWWADMIN%\language\de-DE\de-DE.%%i.ini
  
  ECHO REM mklink %WWWADMIN%\language\de-DE\de-DE.%%i.ini %GIT%com_joomleague\administrator\components\com_joomleague\modules\%%i\language\de-DE\de-DE.%%i.ini
  REM mklink %WWWADMIN%\language\de-DE\de-DE.%%i.ini %GIT%com_joomleague\administrator\components\com_joomleague\modules\%%i\language\de-DE\de-DE.%%i.ini

  REM .sys.ini
  ECHO del %WWWADMIN%\language\de-DE\de-DE.%%i.sys.ini
  del %WWWADMIN%\language\de-DE\de-DE.%%i.sys.ini
  
  ECHO REM mklink %WWWADMIN%\language\de-DE\de-DE.%%i.sys.ini %GIT%com_joomleague\administrator\components\com_joomleague\modules\%%i\language\de-DE\de-DE.%%i.sys.ini
  REM mklink %WWWADMIN%\language\de-DE\de-DE.%%i.sys.ini %GIT%com_joomleague\administrator\components\com_joomleague\modules\%%i\language\de-DE\de-DE.%%i.sys.ini
 
  del %WWWADMIN%\language\en-GB\en-GB.Bytes*
  del %WWWADMIN%\language\en-GB\en-GB.C
  del %WWWADMIN%\language\de-DE\de-DE.Bytes*
  del %WWWADMIN%\language\de-DE\de-DE.C 

  rmdir /S /Q %WWWADMIN%\modules\Bytes
  ECHO -- unlinked Administrator Module %%i --------------------------------------------
)


set PLG_NAME=joomleague_comments
set PLG_GRP_NAME=content
rmdir /S /Q %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME%
ECHO REM REM mklink /D %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
REM REM mklink /D %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
ECHO REM mklink %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
REM mklink %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
ECHO link com_joomleague\administrator\language\en-GB\en-GB.plg_%PLG_NAME%.ini with GIT
ECHO del %WWW%\administrator\language\en-GB\en-GB.plg_%PLG_NAME%.ini 
del %WWW%\administrator\language\en-GB\en-GB.plg_%PLG_NAME%.ini 
REM mklink %WWW%\administrator\language\en-GB\en-GB.plg_%PLG_NAME%.ini %GIT%\com_joomleague\administrator\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%\language\en-GB\en-GB.plg_%PLG_NAME%.ini
ECHO -- unlinked plugin %PLG_NAME% --------------------------------------------

set PLG_NAME=joomleague_person
set PLG_GRP_NAME=content
rmdir /S /Q %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME%
ECHO REM REM mklink /D %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
REM REM mklink /D %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
ECHO REM mklink %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
REM mklink %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
ECHO link com_joomleague\administrator\language\en-GB\en-GB.plg_%PLG_NAME%.ini with GIT
ECHO del %WWW%\administrator\language\en-GB\en-GB.plg_%PLG_NAME%.ini 
del %WWW%\administrator\language\en-GB\en-GB.plg_%PLG_NAME%.ini 
REM mklink %WWW%\administrator\language\en-GB\en-GB.plg_%PLG_NAME%.ini %GIT%\com_joomleague\administrator\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%\language\en-GB\en-GB.plg_%PLG_NAME%.ini
ECHO -- unlinked plugin %PLG_NAME% --------------------------------------------

set PLG_NAME=joomleague
set PLG_GRP_NAME=search
rmdir /S /Q %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME%
ECHO REM REM mklink /D %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
REM REM mklink /D %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
ECHO REM mklink %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
REM mklink %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
ECHO link com_joomleague\administrator\language\en-GB\en-GB.plg_%PLG_NAME%.ini with GIT
ECHO del %WWW%\administrator\language\en-GB\en-GB.plg_%PLG_NAME%.ini 
del %WWW%\administrator\language\en-GB\en-GB.plg_%PLG_NAME%.ini 
REM mklink %WWW%\administrator\language\en-GB\en-GB.plg_%PLG_NAME%.ini %GIT%\com_joomleague\administrator\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%\language\en-GB\en-GB.plg_%PLG_NAME%.ini
ECHO -- unlinked plugin %PLG_NAME% --------------------------------------------

set PLG_NAME=joomleague_esport
set PLG_GRP_NAME=extension
rmdir /S /Q %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME%
ECHO REM REM mklink /D %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
REM REM mklink /D %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
ECHO REM mklink %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
REM mklink %WWW%\plugins\%PLG_GRP_NAME%\%PLG_NAME% %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%
ECHO link com_joomleague\administrator\language\en-GB\en-GB.plg_%PLG_GRP_NAME%_%PLG_NAME%.ini with GIT
ECHO del %WWW%\administrator\language\en-GB\en-GB.plg_%PLG_GRP_NAME%_%PLG_NAME%.ini 
del %WWW%\administrator\language\en-GB\en-GB.plg_%PLG_GRP_NAME%_%PLG_NAME%.ini 
REM mklink %WWW%\administrator\language\en-GB\en-GB.plg_%PLG_GRP_NAME%_%PLG_NAME%.ini %GIT%com_joomleague\components\com_joomleague\plugins\%PLG_GRP_NAME%\%PLG_NAME%\language\en-GB\en-GB.plg_%PLG_GRP_NAME%_%PLG_NAME%.ini
ECHO -- unlinked plugin %PLG_NAME% --------------------------------------------
